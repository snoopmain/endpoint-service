resource "aws_vpc_endpoint_service" "sql" {
  acceptance_required        = false
  network_load_balancer_arns = [aws_lb.nlb.arn]
}

data "aws_caller_identity" "current" {}

resource "aws_vpc_endpoint_service_allowed_principal" "allowed" {
  for_each = var.allowed
  vpc_endpoint_service_id = aws_vpc_endpoint_service.sql.id
  principal_arn           = "arn:aws:iam::${each.key}:root"
}