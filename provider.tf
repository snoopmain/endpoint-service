provider "aws" {
  region = var.region 
  default_tags {
    tags = {
      Environment = "Production"
      Owner       = "Infrastructure"
      Project     = "Endpoint_service"
    }
 }
}

terraform {
   cloud {
    organization = "mark-snoop"
    workspaces {
    name = "prod-endpoint-service"
    }
   }
 }

