variable "region" {
    default = "ap-southeast-2"
}

variable "vpc" {
}

variable "subnet1" {
}

variable "subnet2" {
}

# Instance id OR
# IP address also need to set target_type to ip
variable "server" {    
}

variable "target_type" {
  default = "instance"
}


variable "port" {
}

variable "allowed" {
  type = set(string)
}