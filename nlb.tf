resource "aws_lb" "nlb" {
  internal = true
  load_balancer_type = "network"
  lifecycle {
    create_before_destroy = true
  }
  subnet_mapping {
    subnet_id = var.subnet1
  }
  subnet_mapping {
    subnet_id = var.subnet2
  }
}

resource "aws_lb_listener" "nlb" {
  load_balancer_arn = aws_lb.nlb.arn
  port              = var.port
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb.arn
  }
}

resource "aws_lb_target_group" "nlb" {
  port        = var.port
  protocol    = "TCP"
  target_type = var.target_type
  vpc_id      = var.vpc
}

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.nlb.arn
  target_id        = var.server
  port             = var.port
}